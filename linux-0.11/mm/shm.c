#include <signal.h>
#include <asm/system.h>
#include <linux/sched.h>
#include <linux/head.h>

#include <sys/stat.h>
#include <a.out.h>
#include <linux/fs.h>
#include <linux/mm.h>

unsigned long sys_shmget(int key, int size, int shmflg)
{
	return get_free_page();
}

unsigned long sys_shmat(unsigned long shmid, const void *shmaddr, int shmflg)
{
	unsigned long addr;

	addr = current->start_code + current->brk + PAGE_SIZE;
	/* need other confirm */
	/* addr /= PAGE_SIZE; */
	/* addr *= PAGE_SIZE; */
	put_page(shmid, addr);
	addr -= current->start_code;
	return addr;
}
