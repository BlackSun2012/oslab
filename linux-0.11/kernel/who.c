/*
 *  linux/kernel/who.c
 *
 *  Copyleft 2012   HJW ID:1100300818
 */
#include <errno.h>
#include <linux/kernel.h>
#include <asm/segment.h>

#define MAX 23

char str[MAX + 1];

int sys_iam(const char *name)
{
    int i;

    for (i = 0; i <= MAX; ++i) {
        str[i] = get_fs_byte(name + i);
        if (str[i] == '\0')
            return i;
    }
    return -EINVAL; 
}

int sys_whoami(char *name, unsigned int size)
{
    int i;
    extern int errno;

    for (i = 0; i <= size; ++i) {
        put_fs_byte(str[i], name + i);
        if (str[i] == '\0')
            return i;
    }
    return -EINVAL;
}
