#include <linux/kernel.h>
#include <linux/sched.h>
#include <asm/segment.h>
#include <asm/system.h>

typedef struct {
	int value;		/* value for current */
	char name[64];		/* name of this semaphore */
	int wait_list[64];	/* pids of tasks sleeping here */
	int front;
	int rear;
	int is_full;
} sem_t;

sem_t *sem_list[64];

/**********************************
 * in case can't use the stdio i  *
 * have to write a strcmp myself  *
 * there is some bug here, but it *
 * dosen't matter :)              *
 **********************************/
int match(char *a, char* b)
{
	int i;

	for (i = 0; a[i] == b[i]; ++i)
		if (a[i] == '\0')
			return 1;
	return 0;
}

/********************************************************
 *  there is something wrong in the kernel at wake_up() *
 *  so i have to rewrite the function wake_up myself    *
 *  Egg hurt !!!                                        *
 ********************************************************/
void my_wake_up(struct task_struct **p)
{
	if (p && *p)
		(**p).state=0;
}

int sys_sem_open(const char *name, unsigned int value)
{
	int i, j, k;
	char c, string[64];

	/* get name */
	for (i = 0; (c = get_fs_byte(name + i)) != '\0' && i < 63; ++i)
		string[i] = c;
	string[i] = c;

	/* find empty room from sem_list[] for new semaphore */
	for (j = 0; j < 64; ++j)
		if (!sem_list[j])
			break;

	if (j < 64) {
		/* check */
		for (k = 0; k < 64; ++k)
			if (match(sem_list[k]->name, string)) {
				/*printk("There is other semaphore have the name %s.\n", sem_list[k]->name);*/
				return k;
			}

		/* initial semaphore */
		sem_list[j] = (sem_t *)malloc(sizeof(sem_t));
		/* optional
		for (k = 0; k < 64; ++k)
			sem_list[j]->wait_list[k] = -1;
		*/
		sem_list[j]->value = value;
		while (i >= 0) {
			sem_list[j]->name[i] = string[i];
			--i;
		}
		sem_list[j]->front = -1;
		sem_list[j]->rear = 0;
		sem_list[j]->is_full = 0;

		/* used for debug
		printk("name:\t%s\n", sem_list[j]->name);
		printk("value:\t%d\n", sem_list[j]->value);
		printk("id:\t%d\n", j);
		printk("front:\t%d\n", sem_list[j]->front);
		printk("rear:\t%d\n", sem_list[j]->rear);
		*/
	} else {
		printk("The sem_list[] is full.\n");
		return -1;
	}
	return j;
}

int sys_sem_wait(int id)
{
	int i;
	cli();
	sem_list[id]->value--;
	if (sem_list[id]->value >= 0) {
		/* used for debug
		printk("Increase:value =\t%d\n", sem_list[id]->value);
		*/
		return 0;
	}

	/* search current task number in task[] */
	for (i = 0; i < NR_TASKS; ++i)
		if (task[i] == current)
			break;
	if (i >= NR_TASKS) {
		printk("Not found task with given id in task[].\n");
		return -1;
	}

	/* check if there is any position for i */
	if (sem_list[id]->is_full) {
		printk("wait_list[] is full.");
		return -1;
	}

	/* put i into wait_list[] */
	sem_list[id]->front = (sem_list[id]->front + 1) % 64;
	if ((sem_list[id]->front + 1) % 64 == sem_list[id]->rear)
		sem_list[id]->is_full = 1;
	sem_list[id]->wait_list[sem_list[id]->front] = i;

	/* sleep_on() */
	sleep_on(task + sem_list[id]->wait_list[sem_list[id]->front]);
	/* used for debug
	printk("sem_list[%d]->name =\t%s\n", id, sem_list[id]->name);
	printk("sem_list[%d]->value =\t%d\n", id, sem_list[id]->value);
	printk("sem_list[%d]->front =\t%d\n", id, sem_list[id]->front);
	printk("sem_list[%d]->rear =\t%d\n", id, sem_list[id]->rear);
	printk("wait_list[%d]->pid =\t%d\n", sem_list[id]->front, task[sem_list[id]->wait_list[sem_list[id]->front]]->pid);
	printk("current->pid =\t%d\n", current->pid);
	*/
	/* used for debug
	printk("Wait:value =\t%d\tsleep at wait_list[%d]\n", sem_list[id]->value, sem_list[id]->front);
	*/
	sti();
	return 0;
}

int sys_sem_post(int id)
{
	int i;

	cli();
	sem_list[id]->value++;
	if (sem_list[id]->value > 0) {
		/* used for debug
		printk("Decrease:value = %d\n", sem_list[id]->value);
		*/
		return 0;
	}

	/* search current task number in task[] */
	for (i = 0; i < NR_TASKS; ++i)
		if (task[i] == current)
			break;
	if (i >= NR_TASKS) {
		printk("Not found task with given id in task[].\n");
		return 0;
	}

	/* check if there is task waiting in the wait_list[] */
	if (!(sem_list[id]->is_full) && ((sem_list[id]->front + 1) % 64 == sem_list[id]->rear)) {
		printk("wait_list[] is empty.\n");
		printk("wait_list->front:\t%d\n", sem_list[id]->front);
		printk("wait_list->rear:\t%d\n", sem_list[id]->rear);
		return -1;
	}

	/* wake_up() */
	my_wake_up(task + sem_list[id]->wait_list[sem_list[id]->rear]);

	/* remove rear from wait_list[] */
	sem_list[id]->rear = (sem_list[id]->rear + 1) % 64;
	sem_list[id]->is_full = 0;

	/* used for debug
	printk("Wait:value =\t%d\tsleep at wait_list[%d]\n", sem_list[id]->value, sem_list[id]->front);
	*/
	sti();
	return 0;
}

int sys_sem_unlink(int id)
{
	/* remember to free */
	free(sem_list[id]);
	sem_list[id] = NULL;
	return 1;
}
