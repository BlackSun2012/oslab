#include <stdarg.h>
#include <sys/types.h>
#include <linux/sched.h>
#include <linux/kernel.h>
#include <asm/segment.h>

int proc_read(int dev, char *buf, int count, off_t *pos)
{
		/*
		char c = 'a';
		put_fs_byte(c, buf);
		put_fs_byte(-1, buf + 1);
		*/
		int i;

		printk("task:\tpid\tstate\tfather\tcounter\tstart time\n");
		for (i = 0; task[i]; ++i) {
			printk("task[%d]:\t%d\t%d\t%d\t%d\t%d\n",
					(int)i, 
					(int)(task[i]->pid), 
					(int)(task[i]->state), 
					(int)(task[i]->father),
					(int)(task[i]->counter),
					(int)(task[i]->start_time));
		}
		return 0;
}
