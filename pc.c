#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>	/* For O_* constants */
#include <sys/stat.h>	/* For mode constants */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

main()
{
	int n, i, j, pid, fd;
	sem_t *mutex, *full, *empty;

	signal(SIGCHLD, SIG_IGN);
	fd = open("buffer", O_RDWR|O_CREAT);
	sem_unlink("Mutex");
	sem_unlink("Full");
	sem_unlink("Empty");
	mutex = sem_open("Mutex", O_CREAT, 0644, 1);
	full = sem_open("Full", O_CREAT, 0644, 0);
	empty = sem_open("Empty", O_CREAT, 0644, 10);
	printf("How many Consumer: ");
	scanf("%d", &n);
	for (i = 0; i < n; ++i) {
		if (pid = fork()) {
			/* 父进程(Producer) */
			/*
			sem_wait(mutex);
			printf("F:This is father, creating child %d\n", pid);	
			fflush(stdout);
			sem_post(mutex);
			*/
		}
		else {
			/* 子进程(Consumer) */
			int buf;
			for (j = 0; /*j < 5*/; ++j) {
				sem_wait(full);
				sem_wait(mutex);
				/*
				printf("C:Chile %d print - %d\n", getpid(), j);
				fflush(stdout);
				*/
				/* Consumer */
				read(fd, &buf, sizeof(int));
				printf("Consumer %d: read %d\n", i, buf);
				fflush(stdout);
				sem_post(mutex);
				sem_post(empty);
			}
			//printf("C:This is child, its return value is %d\n", pid);	
			break;
		}
	}

	if (pid) {
		/* Producer */
		off_t cur;
		for (i = 0; /*i < 10*/; ++i) {
			sem_wait(empty);
			sem_wait(mutex);
			cur = lseek(fd, 0, SEEK_CUR);
			//printf("cur:%ld\n", cur);
			lseek(fd, 0, SEEK_END);
			write(fd, &i, sizeof(int));
			lseek(fd, cur, SEEK_SET);
			//printf("Producer: write %d\n", i);
			//fflush(stdout);
			sem_post(mutex);
			sem_post(full);
		}
		/* 父进程 结束 */
		wait(NULL);
		sem_unlink("Mutex");
		sem_unlink("Full");
		sem_unlink("Empty");
		close(fd);
		printf("F:Now father exit\n");
		fflush(stdout);
	}
	else {
		/* 子进程 结束 */
		printf("C:Now child %d exit\n", getpid());
		fflush(stdout);
	}
	return 0;
}
